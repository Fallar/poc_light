import 'package:flutter/services.dart';

class FlutterMethodChannel {
  static const channelName = 'tefal_poc.flutter.dev/muid'; // this channel name needs to match the one in Native method channel
  late MethodChannel methodChannel;

  static final FlutterMethodChannel instance = FlutterMethodChannel._init();
  FlutterMethodChannel._init();

  void configureChannel() {
    methodChannel = const MethodChannel(channelName);
  }
}