import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:poc_tefal/flutter_channel.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = MethodChannel('tefal_poc.flutter.dev/muid');

  String _sharedMuid = 'Empty';
  String _cookieMuid = 'Empty';
  String _intentMuid = 'Empty';

  Future<void> _getMuidShared() async {
    String muid;
    try {
      final String result = await platform.invokeMethod('getMuid');
      muid = result;
    } on PlatformException catch (e) {
      muid = "Failed to get: '${e.message}'.";
    }

    setState(() {
      _sharedMuid = muid;
    });
  }

  Future<void> _getMuidCookie() async {
    String muid;
    try {
      final String result = await platform.invokeMethod('getCookie');
      muid = result;
    } on PlatformException catch (e) {
      muid = "Failed to get: '${e.message}'.";
    }

    setState(() {
      _cookieMuid = muid;
    });
  }

  Future<void> methodHandler(MethodCall call) async {
    final String uuid = call.arguments;
    switch (call.method) {
      case "showIntentUuid":
        setState(() {
          _intentMuid = uuid;
        });
        break;
      default:
        print('no method handler for method ${call.method}');
    }
  }
  
  @override
  void initState() {
    super.initState();
    platform.setMethodCallHandler(methodHandler);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(_sharedMuid),
            ElevatedButton(
              child: Text('Get stored muid shared'),
              onPressed: _getMuidShared,
            ),
            const SizedBox(height: 30),
            Text(_cookieMuid),
            ElevatedButton(
              child: Text('Get stored muid cookie'),
              onPressed: _getMuidCookie,
            ),
            Text("From Intent"),
            const SizedBox(height: 16),
            Text(_intentMuid),
          ],
        ),
      ),
    );
  }
}
